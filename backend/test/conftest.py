import os

import pytest
from splinter import Browser


class DotDict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


@pytest.fixture
def browser():
    return Browser("phantomjs")


# Using only this make the app use the same DB for all the tests in a module
@pytest.fixture(scope="module")
def elogy():
    os.environ["ELOGY_CONFIG_FILE"] = "../test/config.py"

    # Start the test client
    from src.app import app
    current_app = app.test_client()

    # Bind the client to some "extra" places
    import src.service.entryservice as EntryService
    EntryService.current_app = current_app.application

    return current_app


# Initialize the database if the app is not needed, or just reset it to a clear state if using the app already
# This works only with sqlite DB currently
@pytest.fixture(scope="function")
def db():
    from .config import DB_TYPE, DB_NAME
    from src.db.db import setup_database

    setup_database(DB_TYPE, DB_NAME)

    return
