import pytest
import random

from faker import Faker

from src.service.logbookservice import create_logbook, get_logbook, update_logbook, get_logbook_revision, get_logbooks
from src.service.entryservice import create_entry
from src.service.logbookservice import LogbookRevision

from . import providers

fake = Faker()
fake.add_provider(providers.ElogyProvider)


@pytest.mark.parametrize('name, description', [('Logbook1', 'Description1'), ('Logbook2', 'Description2'), ('Logbook3', 'Description3')])
def test_logbook_create(db, name, description):
    # Verify the creation of the logbook with the service
    logbook = fake.logbook()
    logbook.name = name
    logbook.description = description
    lb = create_logbook(logbook)
    assert lb.name == name
    assert lb.description == description


def test_logbook_descendants(db):
    # Verify the correct connection with the children
    parent1 = create_logbook(fake.logbook())
    parent2 = create_logbook(fake.logbook())
    child1 = create_logbook(fake.logbook(parent_id=parent1))
    child2 = create_logbook(fake.logbook(parent_id=parent1))
    child1child1 = create_logbook(fake.logbook(parent_id=child1))
    desc_ids = parent1.descendants
    assert set(desc_ids) == {parent1, child1, child2, child1child1}


def test_logbook_ancestors(db):
    # Verify the correct connection with the parents
    parent1 = create_logbook(fake.logbook())
    parent2 = create_logbook(fake.logbook())
    child1 = create_logbook(fake.logbook(parent_id=parent1))
    child2 = create_logbook(fake.logbook(parent_id=parent1))
    child1child1 = create_logbook(fake.logbook(parent_id=child1))
    desc_ids = child1child1.ancestors
    assert set(desc_ids) == {parent1, child1, child1child1}


def test_logbook_get(db):
    # Create multiple logbooks and then fetch a specific one by ID
    create_logbook(fake.logbook())
    create_logbook(fake.logbook())
    create_logbook(fake.logbook())
    lb = create_logbook(fake.logbook())
    lb2 = get_logbook(lb.get_id())
    assert lb.name == lb2.name
    assert lb.description == lb2.description


def test_logbook_get_children_count(db):
    # Add a few children logbooks and verify those are fetched
    lb = create_logbook(fake.logbook())
    create_logbook(fake.logbook(parent_id=lb))
    create_logbook(fake.logbook(parent_id=lb))
    create_logbook(fake.logbook())
    lb2 = get_logbook(lb.get_id())
    assert len(lb2.children) == 2


def test_logbook_get_all(db):
    # Create multiple logbooks and then fetch the root (0) with all the logbooks ordered by id
    lb0 = create_logbook(fake.logbook(name='22222'))
    lb1 = create_logbook(fake.logbook(name='00000'))
    lb2 = create_logbook(fake.logbook(name='11111'))
    lb3 = create_logbook(fake.logbook(parent_id=lb0, name='00000'))
    lb4 = create_logbook(fake.logbook(parent_id=lb0, name='11111'))
    lb5 = create_logbook(fake.logbook(parent_id=lb1))
    lb6 = create_logbook(fake.logbook(parent_id=lb2))
    lb7 = create_logbook(fake.logbook(parent_id=lb6))
    lb8 = create_logbook(fake.logbook(parent_id=lb7))
    # Structure (lb0 (lb3, lb4), lb1(lb5), lb2(lb6(lb7(lb8))))
    logbook_list = get_logbooks()
    # Expect 3 top level logbooks
    assert len(logbook_list) == 3
    assert logbook_list[0] == lb0
    assert logbook_list[1] == lb1
    assert logbook_list[2] == lb2

    assert len(logbook_list[1].children) == 1
    assert logbook_list[1].children[0] == lb5

    assert logbook_list[2].children[0] == lb6
    assert logbook_list[2].children[0].children[0] == lb7
    assert logbook_list[2].children[0].children[0].children[0] == lb8

    assert len(logbook_list[0].children) == 2
    assert logbook_list[0].children[0] == lb3
    assert logbook_list[0].children[1] == lb4


def test_logbook_get_specific(db):
    # Similar to the previous test but fetch a logbook in the middle
    lb0 = create_logbook(fake.logbook(name='22222'))
    lb1 = create_logbook(fake.logbook(name='00000'))
    lb2 = create_logbook(fake.logbook(name='11111'))
    lb3 = create_logbook(fake.logbook(parent_id=lb0, name='00000'))
    lb4 = create_logbook(fake.logbook(parent_id=lb0, name='11111'))
    lb5 = create_logbook(fake.logbook(parent_id=lb1))
    lb6 = create_logbook(fake.logbook(parent_id=lb2))
    lb7 = create_logbook(fake.logbook(parent_id=lb6, name='00000'))
    lb8 = create_logbook(fake.logbook(parent_id=lb7))
    lb9 = create_logbook(fake.logbook(parent_id=lb6, name='11111'))
    # Structure (lb0 (lb3, lb4), lb1(lb5), lb2(lb6(lb7(lb8))))
    logbook = get_logbook(lb6.get_id())
    # Expect 3 top level logbooks
    assert logbook.parent_item['id'] == 3
    assert len(logbook.children) == 2

    assert logbook == lb6
    assert logbook.children[0] == lb7
    assert logbook.children[0].children[0] == lb8

    assert logbook.children[1] == lb9


def test_logbook_create_entry(db):
    # Verify that the logbook is correctly tied to the entries
    lb = create_logbook(fake.logbook())
    j = random.randrange(5, 15, 1)
    for i in range(0, j):
        create_entry(fake.entry(), logbook_id=lb.get_id())
    assert len(lb.entries) == j


def test_logbook_update(db):
    lb = create_logbook(fake.logbook())
    original = lb.name
    # to properly update the logbook, use the "make_change" method
    # which creates a revision.
    revision = update_logbook(lb.get_id(), name='Logbook2')
    # remember to save both logbook and revision
    lb.save()
    revision.save()

    assert len(lb.changes) == 1
    rev = lb.changes[0]
    assert lb == revision
    # old value is stored in the revision
    assert rev.changed["name"] == original


def test_logbook_revision_wrapper_1(db):
    lb = create_logbook(fake.logbook())
    original = lb.name
    update_logbook(lb.get_id(), name="Logbook2").save()
    lb.save()

    # The wrapper should look like a historical version of  a Logbook
    wrapper = get_logbook_revision(lb, version=0)
    assert wrapper.name == original


def test_logbook_revision_wrapper_2(db):
    lb = create_logbook(fake.logbook())
    original = lb.name
    lb = update_logbook(lb.get_id(), name="Logbook2")
    lb.save()
    lb = update_logbook(lb.get_id(), name="Logbook3")
    lb.save()

    wrapper = get_logbook_revision(lb, version=0)
    assert isinstance(wrapper, LogbookRevision)
    # Check the revision number
    assert wrapper.revision_n == 0
    # Verify the name is what is expected
    assert wrapper.name == original
    # Verify that it is different compared to the new one
    assert wrapper.name != lb.name
    # The description should always be unchanged
    assert wrapper.description == lb.description

    wrapper = get_logbook_revision(lb, version=1)
    assert isinstance(wrapper, LogbookRevision)
    assert wrapper.revision_n == 1
    assert wrapper.name == "Logbook2"
    assert wrapper.name != lb.name
    assert wrapper.description == lb.description

    wrapper = get_logbook_revision(lb, version=2)
    assert wrapper == lb  # newest revision is just the Logbook
    assert wrapper.revision_n == 2
    assert wrapper.name == "Logbook3"
    assert wrapper.description == lb.description


def test_logbook_revision_wrapper_3(db):
    lb = create_logbook(fake.logbook())
    name1 = lb.name
    description1 = lb.description
    name2 = "Name2"
    description2 = "New description"
    lb = update_logbook(lb.get_id(), name=name2, description=description2)
    lb.save()
    lb = update_logbook(lb.get_id(), name=name1)
    lb.save()

    wrapper = get_logbook_revision(lb, version=0)
    assert wrapper.name == name1
    assert wrapper.description == description1

    wrapper = get_logbook_revision(lb, version=1)
    assert wrapper.name == name2
    assert wrapper.description == description2

    wrapper = get_logbook_revision(lb, version=2)
    assert wrapper.name == name1
    assert wrapper.description == description2
    assert wrapper == lb  # newest revision is just the Logbook
