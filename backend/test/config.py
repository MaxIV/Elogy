# This is a config file for testing purposes, it creates a temporary
# file for the database.

from tempfile import NamedTemporaryFile

TITLE = "elogy"

SECRET_KEY = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
SECRET = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

DEBUG = False

# The name of the database file
with NamedTemporaryFile(delete=False) as f:
    DATABASE = f.name

# The folder where all uploaded files will be stored.
UPLOAD_FOLDER = '/tmp/test_elogy'


# Don't change anything below this line unless you know what you're doing!
# ------------------------------------------------------------------------

DB_TYPE = 'sqlite'  # [postgresql, sqlite]
DB_HOST = None  # !!!Do not use /tmp for anything beyond testing!!!
DB_NAME = ':memory:'  # !!!Do not use /tmp for anything beyond testing!!!
DB_USER = None  # !!!Do not use /tmp for anything beyond testing!!!
DB_PASS = None  # !!!Do not use /tmp for anything beyond testing!!!
