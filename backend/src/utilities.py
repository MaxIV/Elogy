from datetime import datetime
from html.parser import HTMLParser

import json
from peewee import DateTimeField
from playhouse.postgres_ext import JSONField

from dateutil.parser import parse
from flask import request, current_app
from flask.json import JSONEncoder
import peewee
import requests
from flask_restful import reqparse
from playhouse.shortcuts import model_to_dict


def get_user_groups(as_sql_list=False):
    """
    Returns the user groups the requesting user belongs to, or an empty list if no or an invalid jwt token is found
    If as_sql_list, returns a string on the format 
    ('g_1', 'g_2', ... 'g_n') to be used directly in a sql query
    """
    try:
        parser = reqparse.RequestParser()
        parser.add_argument('jwt', location='cookies')
        args = parser.parse_args()
        jwt = args["jwt"]
        if not jwt:
            if as_sql_list:
                return "('')" 
            return []
        r = requests.post(
            url=current_app.config["JWT_DECODE_URL"], data={"jwt": jwt})

        if r.status_code != 200:
            if as_sql_list:
                return "('')"
            return []

        data = r.json()
        if not as_sql_list:
            return data["groups"]
        return "('', '" + "', '".join(data["groups"]) + "')"
    except Exception:
        if as_sql_list:
            return "('')"
        return []
    

def request_wants_json():
    "Check whether we should send a JSON reply"
    best = request.accept_mimetypes \
        .best_match(['application/json', 'text/html'])
    print(best)
    print(request.accept_mimetypes[best],
          request.accept_mimetypes['text/html'])

    return best == 'application/json' and \
        request.accept_mimetypes[best] >= \
        request.accept_mimetypes['text/html']


class CustomJSONEncoder(JSONEncoder):
    """JSON serializer for objects not serializable by default json code"""

    def default(self, obj):
        if isinstance(obj, datetime):
            serial = obj.timestamp()
            return serial
        elif isinstance(obj, peewee.SelectQuery):
            print("select")
            return list(obj)
        elif isinstance(obj, peewee.Model):
            serial = model_to_dict(obj, recurse=False)
            return serial

        return JSONEncoder.default(self, obj)


def get_utc_datetime(date_string):
    timestamp = parse(date_string)
    # we want to store UTC since SQLite does not store the TZ
    # information.
    utc_offset = timestamp.utcoffset()
    if utc_offset:
        timestamp -= utc_offset
    # turn our timestamp into a "naive" datetime object
    return timestamp.replace(tzinfo=None)


class CustomJSONField(JSONField):

    def db_value(self, value):
        if value is not None:
            return json.dumps(value, cls=CustomJSONEncoder)


class UTCDateTimeField(DateTimeField):
    """
    A field that stores datetime objects as UTC by recalculating
    the timestamp and removing the timezone info. This is because
    sqlite doesn't really handle timezone info.
    """

    def db_value(self, value):
        if value is None:
            return
        # Note: There are probably neater ways to do this
        utc_offset = value.utcoffset()
        if utc_offset:
            value -= utc_offset
        return super().db_value(value.replace(tzinfo=None))


class MLStripper(HTMLParser):

    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def convert_attributes(logbook, attributes):
    converted = {}
    for name, value in attributes.items():
        try:
            converted[name] = logbook.convert_attribute(name, value)
        except ValueError:
            pass
    return converted


def escape_string(s):
    "Double single quotes for sqlite"
    return s.replace("'", "''")
