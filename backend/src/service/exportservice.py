import time
import base64
import os
from flask import current_app
import re
import zipfile

from src.service.logbookservice import get_logbook, get_all_attachments, get_all_entries
from src.service.entryservice import get_entry
from src.service.attachmentsservice import export_all_attachments, get_attachments

HTML_HEADER = "<html><head><meta charset='utf-8'><style>a{color: #0c879e}; a:hover{color: #0b9ab5} table{border: 1px solid #aaa; border-collapse: collapse;} th{border: 1px solid #aaa;} td{border: 1px solid #aaa;} ul{background: #f0f0f0; padding: 1em 3em;} a[href]:hover{text-decoration: underline} a[href] {text-decoration: none; color: #3962a5} h3 {margin: 0em 0em 0.3em 0em; display: inline-block;} h4 {margin: 0em 0em 0.3em 0em; display: inline-block;} body {font-family: Avenir, Helvetica, Arial; sans-serif;} .float-right {float: right} .container{max-width: 1200px; margin: auto;} .content {padding: 1em} .attachments {border: 2px dashed #ccc; padding: 10px 8px 8px 8px;} .filename {font-weight: bold} .inline-image {max-width: 100%} .followup{border-left: 5px solid #ccc; margin-left: 0.5em; padding-left: 0.5em} .subtitle {font-size: 0.9em;} .meta{color: #005867; margin-top: 0.5em; border-radius: 1px; border: 1px solid #d3e3e4; background: #e7f0f1; padding: 0.5em;} .entry{background: #fff} .timestamp {font-size: 0.8em;} .subtitle-attachment{font-size: 0.9em; padding-bottom:4px} </style></head><body><div class='container'>"
HTML_FOOTER = "</div></body></html>"


def export_logbook_as_html(logbook_id, include_attachments):
    """
    Exports a logbook as a html file
    """
    logbook = get_logbook(logbook_id)
    table_of_content = "<div><h2>Logbook " + logbook.name + "</h2><ul>"
    html_body = ""
    for entry in get_all_entries(logbook_id):
        html_body = html_body + get_entries_as_html(entry, include_attachments)
        table_of_content = table_of_content + "<li><a href='#" + str(entry.id) + "'>" + (entry.title or "(No title)") + "</a> </li>"
    table_of_content = table_of_content + "</ul></div>"
    file_id = str(logbook_id) + '_' + str(int(round(time.time() * 1000)))
    html_file = '/tmp/' + file_id + '.html'
    with open(html_file, 'w') as f:
        f.write(HTML_HEADER + table_of_content + html_body + HTML_FOOTER)
    if include_attachments:
        zip_file = "/tmp/" + file_id + ".zip"
        zip_html_and_attachments(html_file, "logbook_" + str(logbook.id) + '.html', get_all_attachments(logbook_id), zip_file)
        return zip_file
    return html_file


def export_entry_as_html(entry_id, include_attachments, followups=True):
    """
    Exports an elogy entry as a pdf
    """
    entry = get_entry(entry_id)
    file_id = str(entry_id) + '_' + str(int(round(time.time() * 1000)))
    html_file = '/tmp/' + file_id + '.html'
    with open(html_file, 'w') as f:
        f.write(HTML_HEADER + get_entries_as_html(entry, include_attachments) + HTML_FOOTER)
    if include_attachments:
        zip_file = "/tmp/" + file_id + ".zip"
        zip_html_and_attachments(html_file, "entry_" + str(entry.id) + '.html', export_all_attachments(entry, followups), zip_file)
        return zip_file
    return html_file


def get_attachment_base64(attachment_path):
    if attachment_path is not None and not attachment_path.startswith("/"):
        attachment_path = "/" + attachment_path
    filename = current_app.config["UPLOAD_FOLDER"] + attachment_path
    # filename = os.path.join(current_app.config["UPLOAD_FOLDER"], attachment_path)
    # print(filename, file=sys.stdout)
    try:
        with open(filename, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode('ascii')
    except Exception:
        return ""


def get_entries_as_html(entry, include_attachments):
    """
    Exports an elogy entry as a html
    """
    base_url = current_app.config["BASEURL"]

    entry_data = get_entry_as_html(entry, include_attachments)
    entry_html = "<div class='meta'>" \
                 "<h3><a name='{id}'>{title}</a></h3>" \
                 "<div class='subtitle float-right'>" \
                 "<a href=\"{base_url}/logbooks/{logbook_id}/entries/{id}/\">{base_url}/logbooks/{logbook_id}/entries/{id}/</a>" \
                 "</div>" \
                 "<div class='subtitle'>Created {created_at} by {authors}</div>" \
                 "</div>" \
                 "<div class='content'>{content}</div>".format(title=entry.title or "(No title)",
                authors=", ".join(a["name"] for a in entry_data.authors),
                created_at=entry_data.created_at.strftime('%Y-%m-%d %H:%M'),
                id=entry_data.id,
                logbook_id=entry_data.logbook_id,
                content=entry_data.new_content or "---",
                base_url=base_url)
    for followup in entry_data.followups:
        followup_data = get_entry_as_html(followup, include_attachments)
        entry_html = entry_html + "<div class='followup'><div class='meta'><h4><a name='{id}'>{title}</a></h4><div class='subtitle'>Followup created {created_at} by {authors}</div></div><div class='content'>{content}</div></div>".format(
                title=followup_data.title or '',
                id=followup_data.id,
                authors=", ".join(a["name"] for a in followup_data.authors),
                created_at=followup_data.created_at.strftime('%Y-%m-%d %H:%M'),
                content=followup_data.new_content or "---")
    # We now ignore the attachments in the html file, since they're added to the zip file instead
    # attachments = entry.get_attachments()
    # filtered_attachments = [x for x in attachments if x.content_type is not None and x.content_type.startswith("image")]
    # if filtered_attachments:
    #     entry_html = entry_html + "<div class='attachments'>Attachments</div>"
    # for attachment in filtered_attachments:
    #     entry_html = entry_html + "<img class='inline-image' src='data:image/png;base64, " + str(get_attachment_base64(attachment.path)) + "'/>"
    return "<div class='entry'>" + entry_html + "</div>"


def get_entry_as_html(entry, include_attachments):
    """
    Exports an elogy entry as a html
    """
    # content should never be None but some older entries are
    new_content = entry.content or ''
    attach_sources = re.findall('src="/attachments([^"]+)"', new_content)
    # attach_sources = re.findall('(src="/attachments([^"]+)")|(src=\'/attachments([^"]+)\')', entry.content)

    if include_attachments:
        base_src = 'attachments/'
        for attach_source in attach_sources:
            # With the attachment added to the folder it is possible to show it without converting and injecting
            new_content = new_content.replace('src="/attachments' + attach_source.rsplit('/', 1)[0] + '/',
                                              'class="inline-image" src="' + base_src + 'inline/')

            # Fix the link to the images in the zip file
            new_content = new_content.replace('href="/attachments' + attach_source.rsplit('/', 1)[0] + '/',
                                              'href="' + base_src + 'inline/')

        # Add non embedded attachments to the bottom
        extra_attachments = get_attachments(entry)
        attachment_element = '<div class="attachments"><div class="subtitle-attachment">Attachments</div>'
        attachment_div = '<div class ="attachment" title="{title}"><div><div style="display: inline-block; vertical-align: top; margin-left: 0.2em; max-width: 80%;"><span class="filename"><a href="{link}">{filename}</a></span> - <span class="timestamp">{timestamp}</span></div></div></div>'
        for extra_attachment in extra_attachments:
            attachment_element += attachment_div.format(
                title=extra_attachment.filename,
                link=base_src + extra_attachment.link.rsplit('/', 1)[1],
                filename=extra_attachment.filename,
                timestamp=extra_attachment.timestamp.strftime('%Y-%m-%d %H:%M')
            )
        attachment_element += '</div>'
        if extra_attachments:
            new_content += attachment_element
    else:
        for attach_source in attach_sources:
            # Fix the inline images when they are not added to the folder
            img_base64 = get_attachment_base64(attach_source)
            new_content = new_content.replace('src="/attachments' + attach_source, 'class="inline-image" src="data:image/png;base64, ' + img_base64)
            new_content = new_content.replace('href="/attachments' + attach_source, 'style="pointer-events: none" href="' + attach_source)

    entry.new_content = new_content
    return entry


def zip_html_and_attachments(html_file, html_file_name, attachments, destination):
    root_dir = current_app.config["UPLOAD_FOLDER"]
    with zipfile.ZipFile(destination, mode='w') as zip_file:
        for attachment in attachments:
            try:
                filename = os.path.join(root_dir, attachment.path)
                name = filename[filename.rfind("/") + 1:]
                if 'inline' in name:
                    zip_file.write(filename, 'attachments/inline/' + filename[filename.rfind("/") + 1:])
                else:
                    zip_file.write(filename, 'attachments/' + filename[filename.rfind("/") + 1:])
            except Exception:
                pass
            
        zip_file.write(html_file, html_file_name)
