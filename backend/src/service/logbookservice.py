import logging

from peewee import fn, JOIN

from src.authentication import has_access
from src.db.model.attachment import Attachment
from src.db.model.entry import Entry
from src.db.model.logbook import Logbook
from src.db.model.logbookchange import LogbookChange
from src.exceptions import AccessException


def get_logbooks(logbook_id=None, archived=False):
    LogbookParent = Logbook.alias('LogbookParent')
    query = Logbook.select(Logbook, LogbookParent).join(LogbookParent, JOIN.LEFT_OUTER, on=(LogbookParent.id == Logbook.parent)).order_by(Logbook.id.asc())
    if not archived:
        # Remove archived logbooks
        query = query.where(~Logbook.archived)

    # TODO: Improve the logic here, still it's already faster than consecutive queries
    logbooks = []
    sup_logbook = {}
    for item in query:
        sup_logbook[item.id] = item
        sup_logbook[item.id].children = []
        if item.parent:
            sup_logbook[item.id].parent_item = {
                "id": item.parent.id,
                "name": item.parent.name
            }

    for item in query:
        if item.parent_id is None:
            logbooks.append(item)
        else:
            if item.parent_id in sup_logbook:
                sup_logbook[item.parent_id].children.append(item)

    if logbook_id and logbook_id in sup_logbook:
        # The ID is supplied and exists
        return sup_logbook[logbook_id]
    elif logbook_id:
        # The ID is supplied but it doesn't exist
        return None

    return logbooks


def get_logbook(logbook_id, revision_n=None):

    # Easiest way to check if a logbook exist
    logbook = Logbook.select().where(Logbook.id == logbook_id).get()
    if not has_access(user_group=logbook.user_group):
        raise AccessException("You don't have access to this logbook")

    # If the current logbook is archived I want this to see also other archived logbooks
    logbook = get_logbooks(logbook_id, archived=logbook.archived)

    if revision_n is not None:
        return get_logbook_revision(logbook, revision_n)
    return logbook


def get_logbook_revision(logbook, version):
    if version == logbook.revision_n:
        return logbook
    if 0 <= version < logbook.revision_n:
        return LogbookRevision(logbook.changes[version])
    raise LogbookChange.DoesNotExist


def get_logbook_changes(logbook_id):
    logbook = get_logbook(logbook_id)
    return logbook.changes


def get_all_entries(logbook_id):
    if not has_access(logbook_id=logbook_id):
        raise AccessException("You don't have access to this logbook")
    return Entry.select().where(Entry.logbook_id == logbook_id).order_by(Entry.last_changed_at.desc())


def get_all_attachments(logbook_id):
    return Attachment.select().join(Entry, on=(Entry.id == Attachment.entry_id)).where(Entry.logbook_id == logbook_id)


def create_logbook(args, logbook_id=None):
    logbook = Logbook(name=args["name"],
                      parent=args.get("parent_id"),
                      description=args.get("description"),
                      template=args.get("template"),
                      attributes=args.get("attributes", []),
                      metadata=args.get("metadata", {}),
                      archived=args.get("archived", False),
                      user_group=args.get("user_group", ''),
                      owner=args.get("owner", ''))
    if logbook_id:
        parent = get_logbook(logbook_id)
        logbook.parent = parent
    logbook.save()
    return logbook


def update_logbook(logbook_id, **values):
    if not values.get("parent_id"):
        # TODO: this is a little ugly, but we can't allow a parent
        # id of 0 since there's no such logbook id. Top-level logbooks
        # should have parent_id NULL (= None).
        values["parent_id"] = None
    else:
        # make sure the parent exists
        Logbook.get(Logbook.id == values["parent_id"])

    logbook = Logbook.get(Logbook.id == logbook_id)

    original_values = {
        attr: getattr(logbook, attr)
        for attr, value in values.items()
        if getattr(logbook, attr) != value
    }

    change = LogbookChange.create(logbook=logbook, changed=original_values)

    for attr, value in values.items():
        setattr(logbook, attr, value)
    logbook.last_changed_at = change.timestamp

    logbook.save()
    return logbook


def entry_histogram():
    "Return a list of the number of entries per day"
    data = (Entry.select(fn.date(Entry.created_at).alias("date"),
                         fn.min(Entry.id).alias("id"),
                         fn.count(Entry.id).alias("count"))
            .group_by(fn.date(Entry.created_at))
            .order_by(fn.date(Entry.created_at)))
    return [(e.date.timestamp(), e.id, e.count) for e in data]


def check_attributes(logbook, attributes):
    required_attributes = set(info["name"] for info in logbook.attributes if info.get("required"))

    if not required_attributes.issubset(set(attributes)):
        raise ValueError("missing required attributes {}".format(required_attributes - set(attributes)))
    converted_attributes = {}
    for name, value in attributes.items():
        try:
            converted_value = convert_attribute(logbook, name, value)
            converted_attributes[name] = converted_value
        except ValueError as e:
            logging.warning("Discarding attribute %s with value %r; %s", name, value, e)
            # TODO: return a helpful error if this fails?
    return converted_attributes


def get_form_attributes(logbook, formdata):
    result = {}
    for attribute in logbook.attributes or []:
        formitem = "attribute-{name}".format(**attribute)
        if attribute["type"] == "multioption":
            # In this case we'll get the data as a list of strings
            value = formdata.getlist(formitem)
        else:
            # in all other cases as a single value
            value = formdata.get(formitem)
        if value:
            result[attribute["name"]] = convert_attribute(logbook, attribute, value)
    return result


def convert_attribute(logbook, name, value):
    "Try to convert an attribute value to the format the logbook expects"
    # Also useful when the logbook configuration may have changed, and
    # trying to access attributes of previously created entries.
    # Not much point in converting them until someone edits the entry.
    # Note: does not exert itself to convert values and will raise
    # ValueError if it fails.
    try:
        for info in logbook.attributes:
            if info["name"] == name:
                break
        else:
            raise KeyError("Unknown attribute %s!" % name)
        if value is None and not info.get("required"):
            # ignore unset values if not required
            raise ValueError("No value")
        if info["type"] == "text":
            return str(value)
        if info["type"] == "number":
            return float(value)
        if info["type"] == "boolean":
            # Hmm... this will almost always be True
            return bool(value)
        if info["type"] == "text" and isinstance(value, list):
            return value[0]
        if info["type"] == "multioption":
            if not isinstance(value, list):
                return [str(value)]
            if isinstance(value, list) and len(value) == 0:
                raise ValueError("Empty multioption")
    except (ValueError, KeyError, IndexError) as e:
        raise ValueError(e)
    return value  # assuming no conversion is needed...


class LogbookRevision:
    """Represents a historical version of a Logbook."""

    def __init__(self, change):
        self.change = change

    def __getattr__(self, attr):
        if attr == "id":
            return self.change.logbook.id
        if attr == "revision_n":
            return list(self.change.logbook.changes).index(self.change)

        if attr in ("name", "description", "template", "attributes",
                    "archived", "parent_id"):
            return self.change.get_old_value(attr)

        return getattr(self.change.logbook, attr)
