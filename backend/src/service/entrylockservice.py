from datetime import datetime

from src.db.model.entrylock import EntryLock

from src.exceptions import LockedException


def get_lock(entry_id):
    # Fetch the lock
    lock = EntryLock.select().where(
        (EntryLock.entry_id == entry_id) &
        (EntryLock.expires_at > datetime.utcnow()) &
        (EntryLock.cancelled_at.is_null())
    ).limit(1)

    # Get the lock if there is a result, otherwise it's None
    lock = lock[0] if len(lock) > 0 else None
    return lock


def get_entry_lock(entry_id, ip=None, acquire=False, steal=False):
    """check if there's a lock on the entry, and if an ip is given
    try to acquire it."""
    # Fetch the lock
    lock = get_lock(entry_id)

    # If the steal flag is set and the lock exist it is removed
    if lock and steal:
        lock.cancel(ip)
        lock = None

    if lock:
        # If lock exist there is a lock and no steal is happening
        if acquire and ip != lock.owned_by_ip:
            # The lock exist and can't be acquired because someone else owns it
            raise LockedException(lock=lock)
        elif acquire and ip == lock.owned_by_ip:
            # The lock exist, and it's owned by the user. Renew the lock
            return EntryLock.create(entry=entry_id, owned_by_ip=ip)
        else:
            # No acquire was attempted, return the lock
            return lock
    elif acquire or steal:
        # There is no lock, or was removed for the steal, and the user want to acquire it
        return EntryLock.create(entry=entry_id, owned_by_ip=ip)

    # There is no lock and the request isn't trying to acquire one
    raise EntryLock.DoesNotExist


def cancel_entry_lock(ip, entry_id=None, lock_id=None):
    # Fetch lock by id or fall back to the entry id
    if lock_id:
        lock = EntryLock.get(EntryLock.id == lock_id)
    else:
        lock = get_entry_lock(entry_id)

    lock.cancelled_at = datetime.utcnow()
    lock.cancelled_by_ip = ip
    lock.save()

    return lock
