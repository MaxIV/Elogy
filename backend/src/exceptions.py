from flask_restful import fields

class AccessException(Exception):
    """ Raised if the user has no access right to the resource"""
    def __init__(self, message=None):
        self.data = {"message": message}
        super().__init__(message)


class ConflictException(Exception):
    def __init__(self, message=None):
        self.data = {"message": message}
        super().__init__(message)


class LockedException(Exception):
    def __init__(self, lock=None):
        # The datetime will be converted in the frontend as long as the timezone information is preserved
        self.data = {"created_at": (fields.DateTime()).format(lock.created_at)}
