from datetime import datetime
from flask import url_for

from peewee import ForeignKeyField
from peewee import (CharField, BooleanField, DateTimeField)
from playhouse.postgres_ext import JSONField

from src.db.db import BaseModel
from src.db.model.entry import Entry


class Attachment(BaseModel):
    """Store information about an attachment, e.g. an arbitrary file
    associated with an entry. The file itself is not stored in the
    database though, only a path to where it's expected to be.
    """

    entry = ForeignKeyField(Entry, null=True, backref="attachments")
    filename = CharField(null=True)
    timestamp = DateTimeField(default=datetime.utcnow)
    path = CharField()  # path within the upload folder
    content_type = CharField(null=True)
    embedded = BooleanField(default=False)  # i.e. an image in the content
    archived = BooleanField(default=False)

    def __str__(self):
        return "[{}] {}".format(self.id, self.filename)

    @classmethod
    def bind_extra_fields(cls):
        """
        Initialize the JSONField using the set value based on the DB_TYPE
        """
        cls._meta.add_field("metadata", BaseModel.JSONField(null=True, default={}))

    @property
    def link(self):
        return url_for("get_attachment", path=self.path)

    @property
    def thumbnail_link(self):
        return url_for("get_attachment", path=self.path) + ".thumbnail"
