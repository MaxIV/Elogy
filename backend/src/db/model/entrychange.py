from datetime import datetime

from peewee import (CharField, TextField, DateTimeField, ForeignKeyField, DoesNotExist, fn)

from src.db.db import BaseModel
from src.db.model.entry import Entry


class EntryChange(BaseModel):
    """
    Represents a change of an entry.

    The nomenclature here is that a *revision* is what an entry looked
    like at at a given point in time, while a *change* happens at a
    specific time and takes us from one revision to the next.

    Counter-intuitively, what's stored here is the *old* entry
    data. The point is that then we only need to store the fields that
    actually were changed! But it becomes a bit confusing when it's
    time to reconstruct an old entry.
    """

    entry = ForeignKeyField(Entry, backref="changes")
    timestamp = DateTimeField(default=datetime.utcnow)
    change_comment = TextField(null=True)
    change_ip = CharField(null=True)

    @classmethod
    def bind_extra_fields(cls):
        cls._meta.add_field("changed", BaseModel.JSONField(default={}))
        cls._meta.add_field("change_authors", BaseModel.JSONField(null=True, default=[]))

    def get_old_value(self, attr):
        """Get the value of the attribute at the time of this revision.
        That is, *before* the change happened."""

        # First check if the attribute was changed in this revision,
        # in that case we return the stored value.
        if attr in self.changed:
            return self.changed[attr]
        # Otherwise, check for the next revision where this attribute
        # changed; the value from there must be the current value
        # at this revision.
        try:
            change = (EntryChange.select().where((EntryChange.entry == self.entry) & (EntryChange.id > self.id)))

            if BaseModel.db_type == 'postgresql':
                change = change.where(fn.json_extract_path_text(EntryChange.changed, fn.variadic('{attr_id}'.format(attr_id='{' + attr + '}'))).is_null(False))
            else:
                change = change.where(fn.json_extract(EntryChange.changed, '$.{attr_id}'.format(attr_id=attr)).is_null(False))

            change = change.order_by(EntryChange.id).get()
            return change.changed[attr]
        except DoesNotExist:
            # No later revisions changed the attribute either, so we can just
            # take the value from the entry
            return getattr(self.entry, attr)

    def get_new_value(self, attr):
        """Get the value of the attribute after this revision happened.
        If it was not changed, it'll just be the same as before."""

        # Check for the next revision where this attribute changed;
        # the value from there must also be the value after this
        # revision.
        try:
            change = (EntryChange.select()
                      .where((EntryChange.entry == self.entry) & (EntryChange.id > self.id)))

            if BaseModel.db_type == 'postgresql':
                change = change.where(fn.json_extract_path_text(EntryChange.changed, fn.variadic('{attr_id}'.format(attr_id='{' + attr + '}'))).is_null(False))
            else:
                change = change.where(fn.json_extract(EntryChange.changed, '$.{attr_id}'.format(attr_id=attr)).is_null(False))

            change = change.order_by(EntryChange.id).get()
            return change.changed[attr]
        except DoesNotExist:
            # No later revisions changed the attribute, so we can just
            # take the value from the entry
            return getattr(self.entry, attr)
