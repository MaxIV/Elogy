"""
The main entrypoint of the Elogy web application
"""
import logging

from time import time
from flask import Flask, current_app, send_from_directory, g, jsonify
from flask.logging import default_handler
from flask_restful import Api

from .api.export import DownloadResource
from .api.errors import errors as api_errors
from .api.logbooks import LogbooksResource, LogbookChangesResource
from .api.entries import (EntryResource, EntriesResource,
                          EntryLockResource, EntryEditedResource, EntryChangesResource)
from .api.auth import AuthResource
from .api.users import UsersResource
from .api.attachments import AttachmentsResource
from src.db.db import setup_database, db


# Configure the main application object
app = Flask(__name__,
            static_folder="frontend/build/static",
            static_url_path="/static")
app.config.from_envvar('ELOGY_CONFIG_FILE')

app.secret_key = app.config["SECRET"]

default_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s:%(levelname)s - %(funcName)s - %(message)s'))
logging.basicConfig(level=app.config.get("LOGGING_LEVEL"))

if app.config.get("LOGGING_LEVEL") == logging.DEBUG:
    logger = logging.getLogger('peewee')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)


setup_database(app.config["DB_TYPE"],
               app.config["DB_NAME"],
               app.config["DB_HOST"],
               app.config["DB_USER"],
               app.config["DB_PASS"])


# API endpoints
api = Api(app, prefix="/api", errors=api_errors)

api.add_resource(LogbooksResource,
                 "/logbooks/",
                 "/logbooks/<int:logbook_id>/",
                 "/logbooks/<int:logbook_id>/revisions/<int:revision_n>/")

api.add_resource(LogbookChangesResource,
                 "/logbooks/<int:logbook_id>/revisions/")

api.add_resource(AuthResource, "/auth/")
api.add_resource(DownloadResource, "/download/")
api.add_resource(EntriesResource,
                 "/logbooks/<int:logbook_id>/entries/")  # GET

api.add_resource(EntryEditedResource, "/entries/<int:entry_id>/edited")
                
api.add_resource(EntryResource,
                 "/entries/<int:entry_id>/",
                 "/entries/<int:entry_id>/revisions/<int:revision_n>",
                 "/logbooks/<int:logbook_id>/entries/",   # POST, PUT
                 "/logbooks/<int:logbook_id>/entries/<int:entry_id>/",
                 "/logbooks/<int:logbook_id>/entries/<int:entry_id>/revisions/<int:revision_n>")

api.add_resource(EntryChangesResource,
                 "/logbooks/<int:logbook_id>/entries/<int:entry_id>/revisions/")

api.add_resource(EntryLockResource,
                 "/logbooks/<int:logbook_id>/entries/<int:entry_id>/lock",
                 "/entries/<int:entry_id>/lock")

api.add_resource(UsersResource,
                 "/users/")

api.add_resource(AttachmentsResource,
                 "/logbooks/<int:logbook_id>/entries/<int:entry_id>/attachments/",
                 "/logbooks/<int:logbook_id>/entries/<int:entry_id>/attachments/<int:attachment_id>",
                 "/attachments/")


# other routes
@app.route('/attachments/<path:path>')
def get_attachment(path):
    return send_from_directory(current_app.config["UPLOAD_FOLDER"], path)


@app.route("/")
@app.route("/<path:path>")
def get_index(path=None):
    """
    This is a 'catch-all' that will catch anything not matched by a
    previous route, always delivering the main index file. This is
    mainly to allow client side routing to use the URL however it
    likes...
    """
    return send_from_directory("frontend/build", "index.html")


@app.errorhandler(422)
def resource_not_found(e):
    """
    Overwrite the standard return for Unprocessable entity
    If there is additional information from flask on which parameter are wrong return them otherwise give
    the standard error so the entry editor can show them instead of simply crashing or not being useful
    """
    if e.data and "messages" in e.data and "json" in e.data["messages"]:
        return jsonify({"messages": e.data["messages"]["json"]}), 422
    else:
        return jsonify(error=str(e)), 422


# add some hooks for debugging purposes
if app.config.get("LOGGING_LEVEL") == logging.DEBUG:
    @app.before_request
    def before_request():
        g.start = time()


    @app.teardown_request
    def teardown_request(exception=None):
        duration = time() - g.start
        current_app.logger.debug("Request took %f s", duration)


# We need to close the connection manually BUT
# if this is a sqlite in memory DB that would break everything (every connection is like a "new" db)
if app.config["DB_TYPE"] == 'postgresql' or app.config["DB_NAME"] != ':memory:':
    @app.teardown_request
    def teardown_request(exception=None):
        # The connection need to be closed every time
        # Often this will not be needed (like for attachments) but wort case it return False
        db.close()


# Allow CORS requests. Maybe we should only enable this in debug mode?
@app.after_request
def per_request_callbacks(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
    response.headers["Expires"] = '0'
    response.headers["Pragma"] = "no-cache"
    return response


def db_close():
    if app.config["DB_TYPE"] == 'sqlite' and app.config["DB_NAME"] == ':memory:':
        db.close()
    return


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000,  # debug=True,
            extra_files=["templates"])  # this makes sure templates are watched
