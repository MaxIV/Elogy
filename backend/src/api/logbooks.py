from flask_restful import Resource, marshal_with, marshal
from webargs.fields import Integer, Str, Boolean, Dict, List, Nested
from webargs.flaskparser import use_args
from src.service.logbookservice import get_logbook, get_logbooks, create_logbook, update_logbook, get_logbook_changes
from ..actions import new_logbook, edit_logbook
from . import fields, send_signal

logbook_base = {
    "id": Integer(allow_none=True),
    "parent_id": Integer(allow_none=True),
    "description": Str(allow_none=True),
    "template": Str(allow_none=True),
    "attributes": List(Nested({
        "name": Str(),
        "type": Str(
            validate=lambda t: t in ["text",
                                     "number",
                                     "boolean",
                                     "option",
                                     "multioption"]),
        "required": Boolean(),
        "options": List(Str(), load_default=None)
    })),
    "metadata": Dict(),
    "template_content_type": Str(),
    "archived": Boolean(load_default=False),
    "user_group": Str(),
    "owner": Str(),
}

logbook_args = {**logbook_base, "name": Str(required=True, validate=lambda a: len(a.strip()) > 0)}
logbook_args_put = {**logbook_base, "name": Str()}


class LogbooksResource(Resource):
    "Handle requests for logbooks"

    @use_args({"parent": Integer(), "archived": Boolean(dump_default=False)}, location="query")
    @marshal_with(fields.logbook, envelope="logbook")
    def get(self, args, logbook_id=None, revision_n=None):
        """
        Get logbooks, either return a specific logbook or the root view
        When fetching a specific logbook the logbook id has priority on the parent as only 1 should be used
        """
        logbook_id = logbook_id or args.get("parent")
        archived = args.get("archived")
        if logbook_id:
            # In general opening a specific logbook return it also if archived, maintaining this 'feature'
            return get_logbook(logbook_id, revision_n)

        logbooks = get_logbooks(archived=archived)

        return dict(children=logbooks)

    @send_signal(new_logbook)
    @use_args(logbook_args, location="json")
    @marshal_with(fields.logbook, envelope="logbook")
    def post(self, args, logbook_id=None):
        """
        Create a new logbook
        """
        return create_logbook(args, logbook_id)

    @send_signal(edit_logbook)
    @use_args(logbook_args_put, location="json")
    @marshal_with(fields.logbook, envelope="logbook")
    def put(self, args, logbook_id):
        "Update an existing logbook"
        return update_logbook(logbook_id, **args)


class LogbookChangesResource(Resource):

    @marshal_with(fields.logbook_changes)
    def get(self, logbook_id):
        changes = get_logbook_changes(logbook_id)
        return {"logbook_changes": changes}
